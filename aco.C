#include <gecode/search.hh>
#include "meta_aco.h"

namespace Gecode 
{ 
    namespace Search 
    {
        template<template<class> class E, class T>
        E<T>*  aco(T* s, size_t sz, E<T>* e, Search::Statistics& st, const Options& o) 
        {
#ifdef GECODE_HAS_THREADS
            Options to = o.expand();
            return new Meta::ACO<T>(s,sz,e,st,to);
#else
            return new Meta::ACO<T>(s,sz,e,st,o);
#endif
        }
    }
}

// STATISTICS: search-other
