#ifndef __GECODE_SEARCH_META_ACO_HH__
#define __GECODE_SEARCH_META_ACO_HH__

#include <gecode/search.hh>
#include <gecode/int.hh>
#include <gecode/minimodel.hh>
#include <numeric>
#include <chrono>
#include "aco.h"
#include "aco_space.h"
#include "aco_pheromone.h"

namespace Gecode 
{ 
    namespace Search 
    { 
        namespace Meta {

            /// Engine for restart-based search
            template <class T>
            class ACO : public Engine 
            {
            private:

                /// The actual engine
                Engine* e;

                /// The root space to create new partial solutions from scratch
                T* root;
                
                /// The best solution that far
                T* best;

                /// The statistics
                Search::Statistics& stats;

                /// The options
                const Options& opt;

                Stop* stop;

                
                /// Whether the slave can be shared with the master
                bool shared;

                /// Empty no-goods (copied from RBS)
                GECODE_SEARCH_EXPORT static NoGoods eng;

                // Samples before updating pheromones
                unsigned int samples;
                
                int best_cost;
              
                mutable Gecode::Rnd r;

            public:

                /// Constructor
                ACO(T*, size_t, Engine* e, Search::Statistics& stats, const Options& opt);

                /// Return next solution (NULL, if none exists or search has been stopped)
                virtual Space* next(void);

                /// Return statistics
                virtual Search::Statistics statistics(void) const;

                /// Check whether engine has been stopped
                virtual bool stopped(void) const;

                /// Reset engine to restart at space \a s
                virtual void reset(Space* s);

                /// Destructor
                virtual ~ACO(void);

                /// Return no-goods
                virtual NoGoods& nogoods(void);
            };

            template <class T>
            NoGoods ACO<T>::eng;

            template <class T>
            NoGoods&  ACO<T>::nogoods(void) 
            {
                return eng;
            }
            
            template <class T>
            Space* ACO<T>::next(void) 
            {
              
                // Get ACO options from space (drivers do not dispatch options to engines)
                const ACOBaseOptions& o = root->aco_options();

                /** Logic: always return an improving solution. If "constrain" is used, every new non-NULL solution
                 * is an improving solution. If "constrain" is not used, we must check, and go on if it is not.
                 */
                while (true)
                {
                    // Clone root space (clean solution) 
                    T* current = dynamic_cast<T*>(root->clone(shared));
                  
                    if (best != NULL)
                      current->post_aco_branching();
                    else
                      current->initial_solution_branching(0);

                    e->reset(current); // In case of reset, the space passed is not cloned by the engine

                    // If specified by the options, constrain the space
                    if (o.constrain() && best != NULL)
                    {
                      std::cerr << best_cost << std::endl;
                        rel(*current, current->cost() < best_cost);
                        //current->constrain(*best);
                    }
                    
                    // Find next according to constraints, using ACO branching
                    T* n = dynamic_cast<T*>(e->next());
                    stats += e->statistics();

                    // If we have a new solution update the pheromones (no matter what)
                    if (n != NULL)
                    {
                        /*
                        std::cerr << "FOUND: ";
                        n->print(std::cerr);
                        std::cerr << std::endl;
                         
                        if (best != NULL)
                            std::cerr << "Best cost: " << best->cost().val() << std::endl;
                        */
                        
                        bool updated_best = false;

                        // If we already had a best
                        if (best != NULL)
                        {
                            // If n is a better solution, replace best, return n
                            if (n->better_than(best))
                            {
                                delete best;
                                best = dynamic_cast<T*>(n->clone(shared));
                                updated_best = true;
                            }

                            // Otherwise, keep trying (possibly with changed solution)
                            
                        }
                        else
                        {
                            // First solution (bound cost, since first solution is with safe brancher)
                            best = dynamic_cast<T*>(n->clone(shared));
                            best_cost = best->cost().val();
                            updated_best = true;
                        }
                      
                        // Overwrite cost every time a new best is found
                        if(o.constrain_on_best())
                          best_cost = best->cost().val();

                        // Push solution on pheromone update stack
                        samples++;
                        std::cerr << "." << std::endl;
                      
                        ACOPheromone<T>::push(n, o.ants());

                        // If all ants have found their solution, update Ăpheromones
                        
                        
                        if ((samples == o.ants() && !o.update_on_best()) || (updated_best && o.update_on_best()))
                        //if(updated_best)
                        {
                            // Best cost at moment of upgrade
                            best_cost = best->cost().val();
                            
                            // std::cerr << "Pheromone updated." << std::endl;
                            ACOPheromone<T>::update(o.rho());
                            samples = 0;
                        }
                    
                        // Otherwise, continue with next solution
                        if (updated_best)
                            return best->clone();
                    }
                    else
                    {
                        return NULL; 
                    }
                  
                    if (stop != NULL && stop->stop(statistics(), opt))
                    {
                        // Pheromone class takes care of cleaning up solutions
                        if (n != NULL)
                            delete n;
                        return NULL;
                        
                    }
                    
                }

                GECODE_NEVER;

                return NULL;
           
            }

            template <class T>
            Search::Statistics ACO<T>::statistics(void) const 
            {
                return stats + e->statistics();
            }

            template <class T>
            bool ACO<T>::stopped(void) const 
            {
                return e->stopped();
            }

            template <class T>
            void ACO<T>::reset(Space* s) 
            {
                // Reset doesn't clone
                delete root;
                root = dynamic_cast<T*>(s);
            }

            template <class T>
            ACO<T>::~ACO(void) {
                // Deleting e also deletes stop
                delete e;
                delete best;
            }

            template <class T>
            ACO<T>::ACO(T* s, size_t, Engine* e, Search::Statistics& stats, const Options& opt)
            : e(e), root(dynamic_cast<T*>(s->clone())), best(0), stats(stats), opt(opt), stop(opt.stop), shared(opt.threads == 1), samples(0), best_cost(Gecode::Int::Limits::max)
                                                                                                                                                    
            {
                T* current = dynamic_cast<T*>(root->clone(shared));
                current->post_aco_branching();
                e->reset(current);
            }
        }
    }
}


#endif

// STATISTICS: search-other
