#ifndef _ACO_SPACE_H
#define _ACO_SPACE_H

#include <gecode/kernel.hh>
#include <gecode/driver.hh>
#include "aco.h"

class ACOAbstractSpace
{
    public:

        /// The variables to be used to update pheromones
        virtual const Gecode::IntVarArray& vars() const = 0;

        /// Quality function, by default inverse of cost
        virtual double quality() const = 0;

        /// Post ACO brancher on vars()
        virtual void post_aco_branching() = 0;

        /// Checks whether this space is better than another
        virtual bool better_than(const Gecode::Space* o) const = 0;

        /// Pass ACO-specific options
        virtual const Gecode::ACOBaseOptions& aco_options() const = 0;
        
};

class ACOMinimizeScript : public ACOAbstractSpace, public Gecode::MinimizeScript
{
    public:

        virtual double quality() const;

        virtual bool better_than(const Gecode::Space* o) const;
        
    protected:

        ACOMinimizeScript() {}

        ACOMinimizeScript(bool share, ACOMinimizeScript& s) : Gecode::MinimizeScript(share,s) {}
};

#endif

