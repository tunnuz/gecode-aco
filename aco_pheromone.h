#ifndef _ACO_PHEROMONE_H
#define _ACO_PHEROMONE_H

#include <map>
#include <vector>
#include <algorithm>
#include <iostream>

/** Singleton class to manage the parameters of the pheromone model */
template <class T>
class ACOPheromone
{
public:

    static const bool max_cost(const T* a, const T* b)
    {
        return a->cost().val() <= b->cost().val() ? true : false;
    }
    
    /** Push one solution on the update stack. */
    static void push(T* s, unsigned int ants)
    {
        unsigned int lim = u.size()+1;
        while (ants == std::numeric_limits<unsigned int>::max() && lim > ants)
        {
            // Remove worst solution
            auto rem = std::min_element(u.begin(), u.end(), max_cost);
            
            delete (*rem);
            u.erase(rem);
            lim = u.size()+1;
        }
        u.push_back(s);
    }

    /** Update taus. */
    static void update(double rho)
    {
        std::cerr << "Updating taus." << std::endl;
        
        // Preprocessing: find all used components (v,d)
        std::vector<std::pair<unsigned int, unsigned int> > comps;
        std::vector<double> partial_cost;
        double total_cost = 0;

        for (unsigned int si = 0; si < u.size(); si++)
        {
            T& s = *u[si];
            
            for (unsigned int vi = 0; vi < s.vars().size(); vi++)
            {
                // A little overhead in using a vector instead than a set, but easier to handle
                if (!s.vars()[vi].assigned())
                    continue;
                    
                std::pair<unsigned int, unsigned int> c = std::make_pair(vi, s.vars()[vi].val()); 
                unsigned int pos; 

                if (std::find(comps.begin(), comps.end(), c) == comps.end())
                {
                    // Position of component is last element
                    pos = comps.size();
                    comps.push_back(c);
                    partial_cost.push_back(0);
                }
                else
                    pos = std::distance(comps.begin(), find(comps.begin(), comps.end(), c));
                
                // Add solution to list of solutions featuring this component
                partial_cost[pos] += s.quality();
            }

            // Update total cost
            total_cost += s.quality();
        }

        // Update taus
        for(unsigned int ci = 0; ci < comps.size(); ci++)
        {
            std::pair<unsigned int, unsigned int> c = comps[ci];
            tau[c.first][c.second] =
                (1-rho) * get(c.first, c.second) +
                rho * (partial_cost[ci] / total_cost);
            
            // std::cerr << "Updating tau[" << c.first << ", " << c.second << "] = " << tau[c.first][c.second] << std::endl;
        }

        // Count last number of ants
        unsigned int ants = u.size();

        // Possibly, optimize a bit
        for (T* sp : u)
            delete sp;
        
        u.clear();
        u.reserve(ants);
        
        print(std::cerr);

        return;
    }
    
    static void print(std::ostream& os)
    {
    }

    /** Get pheromone value for a given variable/value combination. */
    static double get(unsigned int v, unsigned int d)
    {
        // Grow the pheromone map dynamically
        if (v >= tau.size())
        {
            unsigned int displ = v-tau.size()+1;
            if (displ > 0)
                for (unsigned int g = 0; g < displ; g++)
                    tau.push_back(std::map<unsigned int, double>());
        }
        
        // Return 0.5 if tau is not set
        if (tau[v].find(d) == tau[v].end())
            return 0.5; // tau[v][d] = 0.5;
        return tau[v][d];
    }
    
    static std::map<unsigned int, double> get(unsigned int vi)
    {
        return tau[vi];
    }

protected:

    /** Pheromone store. */
    static std::vector<std::map<unsigned int, double> > tau;

    /** Stack of solutions for next update. */
    static std::vector<T*> u;
};

// Static declarations
template <class T> std::vector<std::map<unsigned int, double> > ACOPheromone<T>::tau;
template <class T> std::vector<T*> ACOPheromone<T>::u;  

#endif

