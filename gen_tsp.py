#!/usr/bin/python
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from random import randint

def main():

    # Add options parser
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--size", "-s", required = True, type=int, help="instance size")
    parser.add_argument("--min-dist", "-mi", required = True, type=int, help="min distance between nodes")
    parser.add_argument("--max-dist", "-ma", required = True, type=int, help="max distance between nodes")
    args = parser.parse_args()

    print args.size
    for i in range(args.size):
        print " ".join(map(str, [randint(args.min_dist, args.max_dist) for j in range(args.size)]))

if __name__ == "__main__":
    main()
