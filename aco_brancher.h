#ifndef _ACO_BRANCHER_H
#define _ACO_BRANCHER_H

#include <gecode/search.hh>
#include <gecode/int.hh>
#include <numeric>
#include <vector>
#include <chrono>

#include "aco_space.h"

/** 
A brancher that uses the pheromone model to decide upon the next value to assign
*/
template <class T>
class ACOBrancher : public Gecode::Brancher
{
public:

    /// Constructor, takes an array of IntViews (variables to branch on)
    ACOBrancher(Gecode::Home home) : Gecode::Brancher(home)
    {
      r.seed(std::chrono::nanoseconds().count());

    }


    /// Helper function to post brancher on space
    static void post(Gecode::Home home)
    {
      (void) new (home) ACOBrancher<T>(home);
    }

    /// Copy constructor
    ACOBrancher(Gecode::Space& home, bool share, ACOBrancher& b) : Gecode::Brancher(home, share, b)
    {
      r.seed(std::chrono::nanoseconds().count());
    }
 
    /// Clone support
    virtual Gecode::Actor* copy(Gecode::Space& home, bool share)
    {
        return new (home) ACOBrancher<T>(home, share, *this);
    }

    virtual bool status(const Gecode::Space& home) const
    {
        // Check whether there is work left
        return !dynamic_cast<const ACOAbstractSpace&>(home).vars().assigned();
    }
    
    virtual const Gecode::Choice* choice(Gecode::Space& home)
    {
        // r.seed(std::chrono::nanoseconds().count());
        Gecode::ViewArray<Gecode::Int::IntView> y(home, static_cast<Gecode::IntVarArgs>(dynamic_cast<T&>(home).vars()));

        std::vector<unsigned int> unassigned;
        for (unsigned int xi = 0; xi < y.size(); xi++)
            if (!y[xi].assigned())
                unassigned.push_back(xi);

        unsigned int xi = unassigned[r(unassigned.size())];
        
        double tau = 0, total_tau = 0;
        std::vector<std::pair<double, int> > p;

        //std::cerr << "Var: " << xi << " ";

        for (Gecode::Int::ViewValues<Gecode::Int::IntView> vi(y[xi]); vi(); ++vi)
        {
            tau = ACOPheromone<T>::get(xi,vi.val());
            total_tau += tau;
            
        //    std::cerr << "(" << vi.val() << ", " << tau << ") ";
            
            p.push_back(std::make_pair(tau, vi.val()));
        }
        //std::cerr << std::endl;

        double pick = ((double)r(RAND_MAX) / (double)RAND_MAX) * total_tau;
        int sel = -1;
      
      
        while(pick > 0)
        {
            ++sel;
            pick -= p[sel].first;
        }
        
        // std::cerr << "Sel: " << sel << " = " << p[sel].second << std::endl;
      
      
        return new ACOChoice(*this, xi, p[sel].second);

        
        
        GECODE_NEVER;
        return NULL;
    }

    virtual Gecode::ExecStatus commit(Gecode::Space& home, const Gecode::Choice& c, unsigned int a)
    {
        const ACOChoice& ac = dynamic_cast<const ACOChoice&>(c);
        Gecode::ViewArray<Gecode::Int::IntView> y(home, static_cast<Gecode::IntVarArgs>(dynamic_cast<T&>(home).vars()));

        Gecode::ExecStatus es;
        
        if (a == 0)
            es = Gecode::me_failed(Gecode::Int::IntView(y[ac.xi]).eq(home, ac.vi)) ? Gecode::ES_FAILED : Gecode::ES_OK;
        else
            es = Gecode::me_failed(Gecode::Int::IntView(y[ac.xi]).nq(home, ac.vi)) ? Gecode::ES_FAILED : Gecode::ES_OK;
        
        // Add solution to updates with very bad grading
      
        //if (es == Gecode::ES_FAILED)
        //    ACOPheromone<T>::push(dynamic_cast<T*>(home.clone()), std::numeric_limits<unsigned int>::max());
        /*
        if (es == Gecode::ES_FAILED)
            std::cerr << "FAILED" << std::endl;
        else
            std::cerr << "OK" << std::endl;
         */
        
        return es;
    }

    virtual const Gecode::Choice* choice(const Gecode::Space& home, Gecode::Archive& e)
    {
        //unsigned int xi, n_vals;
        unsigned int xi;
        int vi;
        e >> xi;
        e >> vi;
        
        /*
        e >> n_vals;
        std::vector<int> v(n_vals, 0);
        for (unsigned int i = 0; i < n_vals; i++)
            e >> v[i];
        */
        return new ACOChoice(*this, xi, vi);
    }

protected:
                                             
    /// Random numbers generator
    mutable Gecode::Rnd r;

    class ACOChoice : public Gecode::Choice
    {   
    public:
        /*
        ACOChoice(const ACOBrancher& b, unsigned int xi, const std::vector<int>& vals)
        : Choice(b, (unsigned int) vals.size()), n_vals(vals.size()), xi(xi)
        */
        ACOChoice(const ACOBrancher& b, unsigned int xi, int vi) : Choice(b, 2), xi(xi), vi(vi)
        {

            //values = Gecode::heap.alloc<int>(n_vals);
            //for (unsigned int i = 0; i < (unsigned int)vals.size(); i++)
            //    values[i] = vals[i];
        }

        virtual size_t size(void) const
        {
            return sizeof(ACOChoice);
            //return sizeof(ACOChoice) + sizeof(int) * n_vals;
        }

        virtual void archive(Gecode::Archive& e) const
        {
            Choice::archive(e);
            e << xi;
            e << vi;
            /*
            e << n_vals;
            for (unsigned int i = 0; i < n_vals; i++)
                e << values[i];
            */
        }

        ~ACOChoice()
        {
            //Gecode::heap.free<int>(values, n_vals);
        }
        
        //unsigned int n_vals;
      
        //int *values;
      
        unsigned int xi;
        int vi;

    };

};

/// Branching function (posts brancher on space)
template <class T>
void ACOBranching(Gecode::Home home)
{
    if (home.failed())
        return;
    ACOBrancher<T>::post(home);
}

#endif
