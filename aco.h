#ifndef __GECODE_ACO_H
#define __GECODE_ACO_H

#include <gecode/kernel.hh>

namespace Gecode 
{
    /**
     * \brief ACO meta-engine interface 
     *
     * \ingroup TaskModelSearch
     */
    template<template<class> class E, class T>
    class ACO : public EngineBase 
    {
    public:

        /// Initialize engine for space \a s and options \a o
        ACO(T* s, const Search::Options& o);

        /// Destructor 
        ~ACO(void);

        /// Return next solution (NULL, if non exists or search has been stopped)
        T* next(void);

        /// Return statistics
        Search::Statistics statistics(void) const;

        /// Check whether engine has been stopped
        bool stopped(void) const;

    protected:

        /// Overall stats
        Search::Statistics stats;

        /// Root space (for cloning)
        T* root;

        /// Sub-engine (we need to pass parameters to this)
        E<T>* engine;

        /// Search options for sub-engine
        const Search::Options& opt;
    };

    /**
     * \brief Activates the search engine on a space, returns new space 
     *
     * \ingroup TaskModelSearch
     */
    template<template<class> class E, class T>
    T* aco(T* s, const Search::Options& o);
}

#include <gecode/search/support.hh>
#include <gecode/driver.hh>

namespace Gecode 
{
    /** 
     * \brief Interface for ACO options (needed?)
     */
    class ACOBaseOptions
    {
    public:
        /// Get evaporation rate 
        virtual double rho() const = 0;

        /// Set evaporation rate
        virtual void rho(double rho) = 0;

        /// Get number of ants
        virtual unsigned int ants(void) const = 0;

        /// Set number of ants
        virtual void ants(unsigned int) = 0;

        /// Get whether the current best solution should be used for constraining the new ones
        virtual bool constrain() const = 0;
        
        /// Set whether the current best solution should be used for constraining the new ones  
        virtual void constrain(bool) = 0;
      
        /// Get whether the pheromone should be updated on new best
        virtual bool update_on_best(void) const = 0;
        
        /// Set whether the pheromone should be updated on new best
        virtual void update_on_best(bool v) = 0;
      
        /// Get whether the solutions should be constrained on new best
        virtual bool constrain_on_best(void) const = 0;
        
        /// Set whether the solutions should be constrained on new best
        virtual void constrain_on_best(bool v) = 0;
    };

    /** Actual options, augments base option class.*/
    template <class OptionsBase>
    class ACOOptions : public ACOBaseOptions, public OptionsBase 
    {
    public:        

        /// Constructor. Declares some parameters needed for aco.
        ACOOptions(const char* p) : 
            OptionsBase(p),
            _rho("-aco_evaporation_rate", "how fast the knowledge based on old solutions is replaced by knowledge based on new solutions", 0.05),
            _ants("-aco_ants", "how many ants are used to build the solutions", 100),
            _constrain("-aco_constrain", "whether the current best solution should be used for constraining the new ones", true),
            _update_on_best("-aco_update_on_best", "when this is set to true (1), taus are updated every time a new best is found", false),
            _constrain_on_best("-aco_constrain_on_best", "whether to constraint after #ants solutions have been generated or at every good solution", false)
        {
            OptionsBase::add(_rho);
            OptionsBase::add(_ants);
            OptionsBase::add(_constrain);
            OptionsBase::add(_update_on_best);
            OptionsBase::add(_constrain_on_best);
        }
      
        /// Get evaporation rate 
        double rho(void) const { return _rho.value(); }

        /// Set evaporation rate
        void rho(double v) { _rho.value(v); }

        /// Get number of ants
        unsigned int ants(void) const { return _ants.value(); }

        /// Set number of ants
        void ants(unsigned int v) { _ants.value(v); }

        /// Get whether the current best solution should be used for constraining the new ones
        virtual bool constrain() const { return _constrain.value(); }
        
        /// Set whether the current best solution should be used for constraining the new ones  
        virtual void constrain(bool v) { _constrain.value(v); }
        
        /// Get whether the pheromone should be updated on new best
        bool update_on_best(void) const { return _update_on_best.value(); }
        
        /// Set whether the pheromone should be updated on new best
        void update_on_best(bool v) { _update_on_best.value(v); }

        /// Get whether the pheromone should be updated on new best
        bool constrain_on_best(void) const { return _constrain_on_best.value(); }
        
        /// Set whether the pheromone should be updated on new best
        void constrain_on_best(bool v) { _constrain_on_best.value(v); }


    protected:

        /// Copy constructor
        ACOOptions(const ACOOptions& opt) : 
            OptionsBase(opt),
            _rho(opt._rho),
            _ants(opt._ants),
            _constrain(opt._constrain),
            _update_on_best(opt._update_on_best),
            _constrain_on_best(opt._constrain_on_best)
        { }

        /// Evaporation rate
        Driver::DoubleOption _rho;

        /// Number of ants
        Driver::UnsignedIntOption _ants;

        /// Whether the current solution must be constrained with the best
        Driver::BoolOption _constrain;
        
        /// Whether the current solution must be constrained with the best
        Driver::BoolOption _update_on_best;

        /// Whether the current solution must be constrained with the best
        Driver::BoolOption _constrain_on_best;
      
    };

    typedef ACOOptions<InstanceOptions> ACOInstanceOptions;
    typedef ACOOptions<SizeOptions> ACOSizeOptions;
}

#include "meta_aco.h"

namespace Gecode 
{
    namespace Search 
    {

        template<template <class> class E, class T>
        forceinline
        Engine* aco(T* s, size_t sz, Engine* e, Search::Statistics& st, const Options& o)
        {
#ifdef GECODE_HAS_THREADS
            Options to = o.expand();
            return new Meta::ACO<T>(s,sz,e,st,to);
#else
            return new Meta::ACO<T>(s,sz,e,st,o);
#endif
        }
    }

    template<template<class> class E, class T>
    forceinline
    ACO<E,T>::ACO(T* s, const Search::Options& opt) : opt(opt) 
    {
        if (opt.clone) {
            if (s->status(stats) == SS_FAILED) {
                stats.fail++;
                // root = new Search::FailedSpace(); // (disappeared between GECODE 4.1.0 and 4.2.0)
                root = NULL;
            } else {
                root = dynamic_cast<T*>(s->clone());
            }
        } else {
            root = dynamic_cast<T*>(s);
        }
        
        // Create new engine based on options, pass on to meta-engine 
        engine = new E<T>(root, opt);
        e = Search::aco<E,T>(root,sizeof(T),engine->e,stats,opt);
    }

    template<template<class> class E, class T>
    forceinline T*
    ACO<E,T>::next(void)
    {
        T* next = dynamic_cast<T*>(e->next());
        return next;
    }

    template<template<class> class E, class T>
    forceinline Search::Statistics
    ACO<E,T>::statistics(void) const {
        return e->statistics();
    }

    template<template<class> class E, class T>
    forceinline bool
    ACO<E,T>::stopped(void) const {
        return e->stopped();
    }

    template<template<class> class E, class T>
    forceinline 
    ACO<E,T>::~ACO(void) {
        if (opt.clone);
            //delete dynamic_cast<T*>(root);
        //delete engine; delete start_engine;
    }

    template<template<class> class E, class T>
    forceinline T*
    aco(T* s, const Search::Options& o) {
        ACO<E,T> l(s,o);
        return l.next();
    }
}

#endif

// STATISTICS: search-other
